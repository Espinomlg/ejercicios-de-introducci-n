package com.example.espino.ejerciciosintroduccion;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Temporizador extends AppCompatActivity {

    private TextView msbtxt, mbbtxt, mtime;
    private Button mbtnpause;
    private AlertDialog.Builder popup;
    private MediaPlayer audio;

    private MyCountDownTimer countdown;
    private int sb, sbValue, minutes, seconds, timerInterval;
    private long millisRemain;
    private boolean paused;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temporizador);

        Bundle bundle = getIntent().getExtras();

        msbtxt = (TextView) findViewById(R.id.smallBlindcount);
        mbbtxt = (TextView) findViewById(R.id.bigBlindCount);
        mtime = (TextView) findViewById(R.id.timeOfGame);
        mbtnpause = (Button) findViewById(R.id.btnPause);
        audio = MediaPlayer.create(Temporizador.this, R.raw.alarma);
        popup = new AlertDialog.Builder(Temporizador.this);

        popup.setTitle(R.string.warning);
        popup.setMessage(R.string.warningtext);
        popup.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                countdown.cancel();
                finish();
            }
        });
        popup.setNegativeButton(R.string.cancel,null);

        sbValue = Integer.valueOf(bundle.getString("blind"));
        sb = sbValue;

        msbtxt.setText(getResources().getString(R.string.smallBlind) + sb);
        mbbtxt.setText(getResources().getString(R.string.bigBlind) + (sb * 2));
        mtime.setText("0:00");

        minutes = 0;
        seconds = 0;
        timerInterval = Integer.valueOf(bundle.getString("time"));

        countdown = new MyCountDownTimer(timerInterval * 60 * 1000, 1000);
        countdown.start();


    }

    public void controlTimer(View view){

        switch (view.getId()){

            case R.id.btnPause:
                if(!paused){
                    countdown.cancel();
                    paused = true;
                    mbtnpause.setText(R.string.Resume);
                }
                else if(paused){
                    countdown = new MyCountDownTimer(millisRemain, 1000);
                    paused = false;
                    mbtnpause.setText(R.string.pauseEnglish);
                    countdown.start();
                }
                break;

            case R.id.btnStop:
                popup.show();
                break;

        }
    }


    private class MyCountDownTimer extends CountDownTimer{

        private MyCountDownTimer(long millisInFuture, long countDownInterval){
            super(millisInFuture,countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            millisRemain = millisUntilFinished;

            if(++seconds == 60){
                minutes++;
                seconds = 0;
            }

            mtime.setText(minutes + ":" + (seconds < 10 ? "0" + seconds : seconds));
        }

        @Override
        public void onFinish() {
            sb += sbValue;

            msbtxt.setText(getResources().getString(R.string.smallBlind)+ sb);
            mbbtxt.setText(getResources().getString(R.string.bigBlind) + (sb * 2));
            audio.start();


            countdown = new MyCountDownTimer(timerInterval * 60 * 1000, 1000);
            countdown.start();
        }
    }
}
