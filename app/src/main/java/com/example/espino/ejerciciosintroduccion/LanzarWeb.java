package com.example.espino.ejerciciosintroduccion;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class LanzarWeb extends AppCompatActivity {

    private EditText murl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lanzarweb);

        murl = (EditText) findViewById(R.id.editWeb);
    }

    public void goToUrl(View view)
    {
        Bundle bundle = new Bundle();
        bundle.putString("url", murl.getText().toString());
        Intent intent = new Intent(LanzarWeb.this, VistaWeb.class);
        intent.putExtras(bundle);

        startActivity(intent);

    }
}
