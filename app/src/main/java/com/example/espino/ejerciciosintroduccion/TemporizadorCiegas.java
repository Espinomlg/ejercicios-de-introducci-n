package com.example.espino.ejerciciosintroduccion;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.NumberPicker;


public class TemporizadorCiegas extends AppCompatActivity {

    private NumberPicker mnpTime, mnpBlind;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temporizadorciegas);

        mnpTime = (NumberPicker) findViewById(R.id.timePicker);
        mnpBlind = (NumberPicker) findViewById(R.id.smallBlindPicker);
        String[] time = {"1", "10", "15", "20"},
                 blind = {"10", "25", "50", "100"};

        mnpTime.setMinValue(0);
        mnpTime.setMaxValue(3);
        mnpTime.setDisplayedValues(time);

        mnpBlind.setMinValue(0);
        mnpBlind.setMaxValue(3);
        mnpBlind.setDisplayedValues(blind);
    }

    public void start(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("blind",mnpBlind.getDisplayedValues()[mnpBlind.getValue()]);
        bundle.putString("time",mnpTime.getDisplayedValues()[mnpBlind.getValue()]);

        Intent intent = new Intent(TemporizadorCiegas.this, Temporizador.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
