package com.example.espino.ejerciciosintroduccion;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;


public class ContadorCafes extends AppCompatActivity {

    private TextView coffeeCount, timer;
    private Button less,more, startBtn;
    private Switch switch1;
    private MediaPlayer audio;
    private AlertDialog.Builder popup;

    private boolean pause,started;
    private long minutes,seconds;
    private byte count;
    private long millisRemain, totalMillis;
    private MyCountDownTimer countdown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contadorcafes);

        coffeeCount = (TextView) findViewById(R.id.txtContador);
        timer = (TextView) findViewById(R.id.Timer);
        less = (Button) findViewById(R.id.bntLess);
        more = (Button) findViewById(R.id.btnMore);
        startBtn = (Button) findViewById(R.id.btnStart);
        switch1 = (Switch) findViewById(R.id.switch1);
        popup = new AlertDialog.Builder(ContadorCafes.this);

        popup.setTitle(R.string.dialogTitle);
        popup.setMessage(R.string.dialogMessage);
        popup.setPositiveButton(R.string.ok, null);

        audio = MediaPlayer.create(ContadorCafes.this, R.raw.alarma);
        minutes = 0;
        seconds = 0;
        count = 0;
        pause = false;
        coffeeCount.setText("" + count);
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.bntLess:
                minutes = minutes > 0 ? --minutes : 0;
                timer.setText(minutes +":00");
                break;
            case R.id.btnMore:
                timer.setText(++minutes + ":00");
                break;
            case R.id.btnStart:

                if (!started && minutes > 0) {
                    if (count < 10) {
                        started = true;
                        less.setEnabled(false);
                        more.setEnabled(false);
                        switch1.setEnabled(false);

                        countdown = new MyCountDownTimer(minutes * 60 * 1000, 1000);
                        countdown.start();
                        minutes = 0;
                        startBtn.setText(R.string.pause);
                    } else {
                        count = 0;
                        coffeeCount.setText("" + count);
                        timer.setText(R.string.start);
                    }
                }
                else if(!pause && started) {
                    pause = true;
                    startBtn.setText(R.string.Continue);
                }
                else if(pause){
                    countdown = new MyCountDownTimer(millisRemain, 1000);
                    countdown.start();
                    pause = false;
                    startBtn.setText(R.string.pause);
                }

        }
    }

    private class MyCountDownTimer extends CountDownTimer{


        private MyCountDownTimer(long millisInFuture, long countDownInterval){
            super(millisInFuture,countDownInterval);
            if(!pause)
                totalMillis = millisInFuture;
        }

        @Override
        public void onTick(long millisUntilFinished) {

            millisRemain = millisUntilFinished;

            if(!switch1.isChecked()){
                minutes = millisUntilFinished /1000 /60;
                seconds = millisUntilFinished /1000 %60;
            }
            else{
                long currentTime = totalMillis - millisUntilFinished;

                minutes = currentTime /1000 / 60;
                seconds = currentTime /1000 % 60;
            }
            timer.setText(minutes +":" + (seconds >= 10 ? seconds : "0" + seconds));

            if(pause)
                this.cancel();

        }

        @Override
        public void onFinish() {
            coffeeCount.setText("" + ++count);
            timer.setText("0:00");
            started = false;
            audio.start();
            minutes = 0;

            if(count == 10){
                startBtn.setText("Reiniciar");
                popup.show();
            }

            switch1.setEnabled(true);
            less.setEnabled(true);
            more.setEnabled(true);
            startBtn.setText(R.string.start);
        }
    }
}
