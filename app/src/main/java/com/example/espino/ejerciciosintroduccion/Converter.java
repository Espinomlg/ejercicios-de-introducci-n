package com.example.espino.ejerciciosintroduccion;

import java.util.Locale;

/**
 * Created by espino on 2/10/16.
 */

public class Converter {

    private static double change;
    private static final double  INCH = 0.393701;

    public Converter(String change)
    {
        this.change = Double.parseDouble(change);
    }
    public Converter()
    {

    }

    public String convertToEuros(String cantidad) {
        double value;

        value = Double.parseDouble(cantidad) * change;

        return String.format(Locale.US,"%.2f", value);

    }
    public String convertToDollars(String cantidad) {
        double value;

        value = Double.parseDouble(cantidad) / change;

        return String.format(Locale.US,"%.2f", value);

    }

    public String convertToInches(String cantidad)
    {
        double value;

        value = Double.parseDouble(cantidad) * INCH;

        return String.format(Locale.US,"%.2f", value);
    }
}

