package com.example.espino.ejerciciosintroduccion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by espino on 2/10/16.
 */
public class VistaWeb  extends AppCompatActivity {

    private WebView mwebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vistaweb);

        Bundle bundle = getIntent().getExtras();

        mwebView = (WebView) findViewById(R.id.webView);

        mwebView.setWebViewClient(new WebViewClient());
        mwebView.loadUrl(bundle.getString("url"));
    }
}
