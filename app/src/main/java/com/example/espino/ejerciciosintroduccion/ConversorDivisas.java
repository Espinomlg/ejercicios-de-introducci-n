package com.example.espino.ejerciciosintroduccion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;



public class ConversorDivisas extends AppCompatActivity {

    private EditText mdollars, meuros, mchange;
    private RadioButton mtoEuros, mtoDollars;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversordivisas);

        mdollars = (EditText) findViewById(R.id.editDollars);
        meuros = (EditText) findViewById(R.id.editEuros);
        mchange = (EditText) findViewById(R.id.change);
        mtoDollars = (RadioButton) findViewById(R.id.toDollars);
        mtoEuros = (RadioButton) findViewById(R.id.toEuros);

    }

    public void convertCoin(View view){

        try {
            if (!mchange.getText().toString().isEmpty()) {

                Converter converter = new Converter(mchange.getText().toString());

                if (mtoEuros.isChecked() && !mdollars.getText().toString().isEmpty())
                    meuros.setText(converter.convertToEuros(mdollars.getText().toString()));

                else if (mtoDollars.isChecked() && !meuros.getText().toString().isEmpty())
                    mdollars.setText(converter.convertToDollars(meuros.getText().toString()));
            }

        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(),"Ha ocurrido un error",Toast.LENGTH_SHORT).show();
        }

    }

}
