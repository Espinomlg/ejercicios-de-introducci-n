package com.example.espino.ejerciciosintroduccion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class ConversorMedidas extends AppCompatActivity {

    private EditText mcentimeters;
    private TextView minches;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversormedidas);

        mcentimeters = (EditText) findViewById(R.id.Centimeters);
        minches = (TextView) findViewById(R.id.inches);

    }

    public void convertMeasure(View view)
    {
        Converter converter = new Converter();

        try {
            if(!mcentimeters.getText().toString().isEmpty())
                minches.setText(converter.convertToInches(mcentimeters.getText().toString()));
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error",Toast.LENGTH_SHORT).show();
        }
    }
}
