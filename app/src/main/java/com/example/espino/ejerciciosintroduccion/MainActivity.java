package com.example.espino.ejerciciosintroduccion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private Intent mintent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void launch(View view)
    {
        switch (view.getId()) {
            case R.id.Ejercicio1:
                mintent = new Intent(MainActivity.this, ConversorDivisas.class);
                break;
            case R.id.Ejercicio2:
                mintent = new Intent(MainActivity.this, ConversorMedidas.class);
                break;
            case R.id.Ejercicio3:
                mintent = new Intent(MainActivity.this, LanzarWeb.class);
                break;
            case R.id.Ejercicio4:
                mintent = new Intent(MainActivity.this, ContadorCafes.class);
                break;
            case R.id.Ejercicio5:
                mintent = new Intent(MainActivity.this, TemporizadorCiegas.class);
                break;
        }

        startActivity(mintent);
    }
}
